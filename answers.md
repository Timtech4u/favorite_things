## Duration

I spent an estimate of between 10-15hours on research and development.
Began working on the backend at PyCon Africa Conference where I met Daniel Roy Greenfeld and worked on the frontend for a while after the conference.
If I had more time, I would have added:
- better validations and error messages
- better algorithm for ranking re-ordering
- a more dynamic frontend single page applicatin


## Feature

*Python 3.6 provides a way for declaring types, examples:*
```
# A dictionary where the keys are strings and the values are ints

friends: Dict[str, int] = {
    "John": 30,
    "Tim": 15
}

# A list of integers

favoriteRankings: List[int] = [1, 2, 3, 4, 5, 6]

# A list that holds dicts that each hold a string key / int value

positions: List[Dict[str, int]] = [
    {"Tim": 1},
    {"John": 2}
]
```

## Performance
Apache HTTP server benchmarking tool is a tool I use in tracking performance in production. I use if to perform stress test.