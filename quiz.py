from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdT5n4isUsvjViJ9WsHWH9tj1Xco50LebwTTwkTalyKUsLG32kf2dTcB0t-WPI80s11CveUKdWCkMZtOxXjO6vIpfm0OgCtRdfSCru--MTonVL_ynx1CyXra36FddWKoSfkR_WrHHCGXpMCQnruOTQKRtrnVYwWIXaJYoZSgmeN49ooCQ='

def main():
    f = Fernet(key)
    print(f.decrypt(message).decode('utf-8'))


if __name__ == "__main__":
    main()