# Favorite Things

>
BriteCore Software Engineer Challenge

## Basic User Flow
*Note that it might take a few seconds for the first time to load URL/data since deployment makes use of Heroku free dynos*
- [Explore Backend API Docs](https://timbritecore.herokuapp.com/api/docs)
- [Visit FrontEnd](https://wonderful-clarke-466ff3.netlify.com/)
- On frontend, to *add new Favorite Thing*, click the button with text: **Click to add new favorite item*
  - Enter values for fields
  - Search/Select for Category
  - If needed, add new Category
- On frontend, to *edit Favorite Thing*, click edit button on the table for desired thing to edit. 
- On frontend, to *delete Favorite Thing*, click delete button on the table for desired thing to edit. 

## Deliverables

### Backend Data

- Python Models containing the ORM classes: [here](./things/models.py)
- Entity Relationship Diagram: [here](./favorite-things-erd.png)
- JSON Representation of Models: [here](./model-json.png)

### Backend API
- [API Codes](./things)
- Django REST Framework Class Based Views: [here](./things/views.py)
- Deployed REST API Documentation: [here](https://timbritecore.herokuapp.com/api/docs)

### Frontend
- [Codes](./frontend/src)
- **Screenshots** [here](#screenshots)
- Deployed Single Page Application: [here](https://wonderful-clarke-466ff3.netlify.com/)

## Technologies Used

### Backend
- Django with Cookiecutter
- Django Rest Framework
- Ubuntu O.S.
- Python 3.7 with PEP8
- PostgreSQL database
- Heroku deployment

### Frontend
- Vue.js
- ESLint
- Vuex State Managment
- Axios
- Element UI
- Netlify's deployment

## [Tests](./things/tests.py)
- To Test Favorite Thing Category Create: `python manage.py test things.tests.TestThingsCategory.test_create`
- To Test Favorite Thing Category List: `python manage.py test things.tests.TestThingsCategory.test_list`
- To Test Favorite Thing Create: `python manage.py test things.tests.TestFavoriteThings.test_create`
- To Test Favorite Thing List: `python manage.py test things.tests.TestFavoriteThings.test_list`
- To Test Favorite Thing Delete: `python manage.py test things.tests.TestFavoriteThings.test_delete`

## Screenshots

### Add new Favorite Thing
![new](screenshots/new.png)

### List of Favorite Things
![list](screenshots/list.png)

### Favorite Things Detail
![detail](screenshots/detail.png)

### Favourite Thing Audit Log
![log](screenshots/log.png)

### Edit Favorite Thing
![edit](screenshots/edit.png)

### Category
![category](screenshots/category.png)
