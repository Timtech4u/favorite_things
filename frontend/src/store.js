import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apiUrl: 'https://timbritecore.herokuapp.com/api/v1',
    currentThing: {
      metadata: []
    },
    thingCategories: [],
    showForm: false,
    action: "",
    allFavoriteThings: ""
  },
  mutations: {
    updateAction(state, action){
      state.action = action
    },
    updateShowForm(state, showForm){
      state.showForm = showForm
    },
    updateCurrentThing(state, thing){
      state.currentThing = thing
    },
    updateThingCategories(state, thingCategories){
      state.thingCategories = thingCategories
    },
    updateFavoriteThings(state, favoriteThings){
      state.allFavoriteThings = favoriteThings
    }
  },
  actions: {
    
  }
})
