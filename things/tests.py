import json
from django.test import TestCase
from rest_framework.test import APIClient

class TestThingsCategory(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.uri = "/api/v1/things-category/"
    
    def test_list(self):
        """
        Ensure we can get a list of favorite things category from API endpoint
        """
        response = self.client.get(self.uri)
        self.assertEqual(response.status_code, 200, 
                "Expected to get Response Code of 200, but received {}"
                    .format(response.status_code))
    
    def test_create(self):
        """
        Ensure we can create a favorite thing category from API endpoint
        """
        params = {
            "name": "FakeCategory",
        }
        response = self.client.post(self.uri, params, format="json")
        self.assertEqual(response.status_code, 201,
                "Expected to get Response Code of 201, but received {}"
                    .format(response.status_code))


class TestFavoriteThings(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.uri = "/api/v1/things/"
    
    def test_list(self):
        """
        Ensure we can get a list of favorite things from API endpoint
        """
        response = self.client.get(self.uri)
        self.assertEqual(response.status_code, 200, 
                "Expected to get Response Code of 200, but received {}"
                    .format(response.status_code))
    
    def test_create(self):
        """
        Ensure we can create a favorite thing from API endpoint
        Firstly, create a fake category and get its name value
        Then use the category name to create Favorite Thing
        """
        category = self.client.post("/api/v1/things-category/",
            {"name": "FakeCategory"}, format="json")
        category_name = category.data["name"]
        params = {
            "title": "FakeFavorite",
            "category": {
                "name": category_name
            },
            "ranking": 1
        }
        response = self.client.post(self.uri, params, format="json")
        self.assertEqual(response.status_code, 201,
                "Expected to get Response Code of 201, but received {}"
                    .format(response.status_code))

    def test_delete(self):
        """
        Ensure we can create a favorite thing from API endpoint
        Firstly, create a fake category and get its name value
        Then use the category name to create Favorite Thing
        Then delete it
        """
        category = self.client.post("/api/v1/things-category/",
            {"name": "FakeCategory"}, format="json")
        category_name = category.data["name"]
        params = {
            "title": "FakeFavorite",
            "category": {
                "name": category_name
            },
            "ranking": 1
        }
        response = self.client.post(self.uri, params, format="json")
        deleteUrl = self.uri + str(response.data["id"]) + "/"
        deleteResponse = self.client.delete(deleteUrl, response.data, format="json")
        # Default implementation of delete returns 204
        self.assertEqual(deleteResponse.status_code, 204,
                "Expected to get Response Code of 204, but received {}"
                    .format(deleteResponse.status_code))
