from django.urls import include, path
from rest_framework import routers
from .views import FavoriteThingView, FavoriteThingCategoryView

app_name = "things"

router = routers.SimpleRouter()
router.register(r"things", FavoriteThingView)
router.register(r"things-category", FavoriteThingCategoryView)

urlpatterns = router.urls
