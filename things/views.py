from rest_framework import viewsets
from .models import FavoriteThing, FavoriteThingCategory
from .serializers import (FavoriteThingSerializer, 
        FavoriteThingCategorySerializer)

class FavoriteThingView(viewsets.ModelViewSet):
    """
        Handles API CRUD Actions for FavoriteThing Model Serializer
    """
    queryset = FavoriteThing.objects.all()
    serializer_class = FavoriteThingSerializer


class FavoriteThingCategoryView(viewsets.ModelViewSet):
    """
        Handles API CRUD Actions for FavoriteThingCategory Model Serializer
    """
    queryset = FavoriteThingCategory.objects.all()
    serializer_class = FavoriteThingCategorySerializer
