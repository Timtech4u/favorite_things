from django.db import models

# Model Additions
from django.core.validators import MinLengthValidator
from django.contrib.postgres.fields import JSONField

# Django Audit App
from simple_history.models import HistoricalRecords

# Django Signals
from django.db.models.signals import pre_save
from django.dispatch import receiver


class FavoriteThing(models.Model):
    """
        Model to store Favorite Things, fields are:
        title (required),
        description (optional, min=10),
        ranking (required & unique integer),
        metadata (optional & contains key/value attributes),
        category (required selection from FavoriteThingCategory Model),
        created / modified date (required & automatically added fields),
        audit log (required, automic & contains string list of changes to data)
    """
    title = models.CharField(max_length=255)
    description = models.CharField(validators=[MinLengthValidator(10)],
                                   max_length=512, null=True, blank=True)
    ranking = models.IntegerField()
    metadata = JSONField(null=True, blank=True)
    category = models.ForeignKey('FavoriteThingCategory',
                                 on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now=True)
    modified_date = models.DateTimeField(auto_now=True)
    audit_log = HistoricalRecords()

    class Meta:
        verbose_name = "Favorite Thing"
        verbose_name_plural = "Favorite Things"
        ordering = ['title']

    def __str__(self):
        return self.title


class FavoriteThingCategory(models.Model):
    """
        Model to store Favorite Thing Categories: name
    """
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Favorite Thing Category"
        verbose_name_plural = "Favorite Thing Categories"
        ordering = ['name']

    def __str__(self):
        return self.name

        
@receiver(pre_save, sender=FavoriteThing)
def update_rankings(sender, instance, *args, **kwargs):
    """
        Updates Ranking across model instances in same category
        - if new ranking does not exist : do nothing
        - else : re-order instance
    """
    category_things = FavoriteThing.objects.filter(
                category=instance.category).order_by('ranking')
    current_rankings = [f.ranking for f in category_things]
    if (instance.ranking in current_rankings):
        instance.ranking += 1
        instance.save()
