from django.contrib import admin
from .models import FavoriteThing, FavoriteThingCategory

admin.site.register(FavoriteThing)
admin.site.register(FavoriteThingCategory)