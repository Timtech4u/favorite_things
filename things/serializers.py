from rest_framework import serializers
from .models import FavoriteThing, FavoriteThingCategory


class AuditLogHistorySerializer(serializers.ListField):
    """
    Serializer for Audit Log History
    """
    child = serializers.DictField()

    def to_representation(self, data):
        return super().to_representation(data.values())


class FavoriteThingCategorySerializer(serializers.ModelSerializer):
    """
        Serializer for FavoriteThingCategory Model
    """
    
    class Meta:
        model = FavoriteThingCategory
        fields = "__all__"


class FavoriteThingSerializer(serializers.ModelSerializer):
    """
        Serializer for FavoriteThing Model
    """
    category = FavoriteThingCategorySerializer()
    audit_log = AuditLogHistorySerializer(read_only=True)

    class Meta:
        model = FavoriteThing
        fields = '__all__'
    
    # Override .create() method to support **category** nested field
    def create(self, validated_data):
        # Get Category name value from OrderedDict and retrieve its object
        category_name = validated_data.pop('category')['name']
        category = FavoriteThingCategory.objects.get(name=category_name)
        new_fav_thing = FavoriteThing(**validated_data, category=category)
        new_fav_thing.save()
        return new_fav_thing

    # Override .update() method
    def update(self, instance, validated_data):
        category_name = validated_data.pop('category')['name']
        category = FavoriteThingCategory.objects.get(name=category_name)
        instance.title = validated_data.pop('title')
        instance.description = validated_data.pop('description')
        instance.ranking = validated_data.pop('ranking')
        instance.metadata = validated_data.pop('metadata')
        instance.category = category
        instance.save()
        return instance